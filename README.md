# Scraper for second-hand bikes sold on tori.fi

A scraper that downloads data about second-hand bikes sold in Finland on [tori.fi](http://www.tori.fi), eventually adding the transformed data to a Google spreadsheet ([see example](https://docs.google.com/spreadsheets/d/1FMjgLm6RL60pnbgGwgyR7OMKvnQ_tO4dIHrMh8d4qdQ/edit?usp=sharing)). The script also downloads the photos locally. 

Created in April 2021 for a Yle story on [bike thefts](https://arenan.yle.fi/1-50677545), however never used in the final television story. 



