 

import requests
import urllib.request
from bs4 import BeautifulSoup
#import csv
import pprint
import datetime
import time
import random
import re

import gspread
from oauth2client.service_account import ServiceAccountCredentials


months_partitive = {"tammikuuta": "01",
	"helmikuuta": "02",
	"maaliskuuta": "03",
	"huhtikuuta": "04",
	"toukokuuta": "05",
	"kesäkuuta": "06",
	"heinäkuuta": "07",
	"elokuuta": "08",
	"syyskuuta": "09",
	"lokakuuta": "10",
	"marraskuuta": "11",
	"joulukuuta": "12"}

months_elative = {"tammikuusta": "01",
	"helmikuusta": "02",
	"maaliskuusta": "03",
	"huhtikuusta": "04",
	"toukokuusta": "05",
	"kesäkuusta": "06",
	"heinäkuusta": "07",
	"elokuusta": "08",
	"syyskuusta": "09",
	"lokakuusta": "10",
	"marraskuusta": "11",
	"joulukuusta": "12"}



urls = [["https://www.tori.fi/uusimaa/polkupyorat_ja_pyoraily/kilpapyorat?ca=18&st=s&cg=4150&c=4152&f=p&w=1&o=",
"kilpapyorat"],
["https://www.tori.fi/uusimaa/polkupyorat_ja_pyoraily/maastopyorat?ca=18&st=s&cg=4150&c=4155&f=p&w=1&o=",
"maastopyorat"],
["https://www.tori.fi/uusimaa/polkupyorat_ja_pyoraily/sahkopyorat?ca=18&st=s&cg=4150&c=4162&f=p&w=1&o=",
"sahkopyorat"],
["https://www.tori.fi/uusimaa/polkupyorat_ja_pyoraily/hybridipyorat?ca=18&st=s&cg=4150&c=4164&f=p&w=1&o=",
"hybridipyorat"],
["https://www.tori.fi/uusimaa/polkupyorat_ja_pyoraily/lasten_pyorat?ca=18&st=s&cg=4150&c=4160&f=p&w=1&o=",
"lasten_pyorat"],
["https://www.tori.fi/uusimaa/polkupyorat_ja_pyoraily/muut_pyorat?ca=18&st=s&cg=4150&c=4165&f=p&w=1&o=",
"muut_pyorat"]]


data = []

for url in urls:

	base_url = url[0]
	category = url[1]

	print(category)

	#get last page pagination
	page = requests.get("{}1".format(base_url))
	soup = BeautifulSoup(page.text, "html.parser")

	lastpage_raw = soup.find_all("div", {"id": "last_page"})
	lastpage_raw_link = lastpage_raw[0].find_all("a")[0]["href"]

	lastpage = int(re.findall(r"([0-9]+)$", lastpage_raw_link)[0])

	for pagination in range(1, lastpage + 1):

		print(pagination)

		page = requests.get("{}{}".format(base_url, pagination))
		soup = BeautifulSoup(page.text, "html.parser")

		items = soup.find_all("a", {"class": "item_row_flex"})

		for item in items:

			item_href = item["href"]
			
			item_id = item["id"].replace("item_", "")

			item_pics = item.find_all("div", {"class": "images-count-container"})[0].get_text().strip()

			if item_pics == "":
				item_pics = 0

			item_dict = {"href": item_href, "id": item_id, "pics_count": item_pics, "category": category, "relevant": ""}
			#print(item_id)

			data.append(item_dict)

		time.sleep(2)

'''

data = [{'category': 'muut_pyorat',
  'href': 'https://www.tori.fi/uusimaa/Naisten_pyora__Raleigh_Lady__Hobby_Hall__valkoinen_56366023.htm?ca=18&w=1',
  'id': '56366023',
  'pics_count': '3'}]
'''

#handle spreadsheet

# define the scope
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']

# add credentials to the account
creds = ServiceAccountCredentials.from_json_keyfile_name('yle-bikes-7935e05cddf2.json', scope)

# authorize the clientsheet 
client = gspread.authorize(creds)

# get the instance of the Spreadsheet
sheet = client.open('Yle_bikes_tori_data')

# get the first sheet of the Spreadsheet
sheet_instance = sheet.get_worksheet(0)

#get all id_numbers that already exist in the spreadsheet
#note that resulting list consists of strings not numbers
existing_ids = [item for item in sheet_instance.col_values(1) if item]
existing_ids = existing_ids[1:] #delete first item which is column header
existing_ids = [int(item) for item in existing_ids] #convert string to int


#new_entry gets either true or false

for item in data:
	if int(item["id"]) not in existing_ids:
		item["new_entry"] = True
	else:
		item["new_entry"] = False



for item in data:

	if item["new_entry"]:

		#print progress
		print("")
		print(item["id"])

		page = requests.get(item["href"])
		soup = BeautifulSoup(page.text, "html.parser")

		#get price and clean it
		try:
			price_raw = soup.find_all("div", {"class": "price"})
			price = price_raw[0].get_text().replace("€", "").strip()

			print(price)
			item["price"] = price

			if "\n" in price:
				temp_split = price.split("\n")
				item["price_now"] = int(temp_split[0].replace(" ", ""))
				item["price_previous"] = int(temp_split[1].replace(" ", ""))
			else:
				item["price_now"] = price.replace(" ", "")
				item["price_previous"] = ""
		except:
			item["price"] = "ERROR"
			item["price_now"] = "ERROR"
			item["price_previous"] = "ERROR"


		#get title
		try:
			title_raw = soup.find_all("h1", {"itemprop": "name"})
			title = title_raw[0].get_text().strip()

			print(title)
			item["title"] = title
		except:
			item["title"] = "ERROR"


		#get published date+time and clean it
		try:
			table_raw = soup.find_all("table", {"class": "tech_data"})
			table_td_raw = table_raw[0].find_all("td", {"class": "value"})

			published_raw = table_td_raw[1].get_text() #14 tammikuuta 22:46
			
			print(published_raw)
			item["published_original"] = published_raw

			published_raw2 = re.search(r'([0-9]+)\s(.*)\s([0-9]+):([0-9]+)', published_raw)
			published_time = "2021-{}-{:02d} {}:{}".format(months_partitive[published_raw2.group(2)], int(published_raw2.group(1)), published_raw2.group(3), published_raw2.group(4))

			#check if date is in the future, then change to previous year
			date_object = datetime.datetime.strptime(published_time, "%Y-%m-%d %H:%M")
			now = datetime.datetime.now()

			if date_object > now:
				published_time = published_time.replace("2021", "2020")

			print(published_time)
			item["published_date_time"] = published_time 
		except:
			item["published_original"] = "ERROR"
			item["published_date_time"] = "ERROR"


		#get tire diameter
		try:
			diameter_raw = table_td_raw[2].get_text()

			if diameter_raw == "-":
				diameter_raw = "N/A"

			print(diameter_raw)
			item["diameter"] = diameter_raw
		except:
			item["diameter"] = "ERROR"


		#get description
		try:
			desc_raw = soup.find_all("div", {"itemprop": "description"})
			desc = desc_raw[0].get_text().replace("Lisätiedot", "").replace("\n", " ").strip()

			print(desc)
			item["description"] = desc
		except:
			item["description"] = "ERROR"


		#get seller name
		try:
			seller_name_raw = soup.find_all("b", {"class": "name"})
			seller = seller_name_raw[0].get_text()

			print(seller)
			item["seller_name"] = seller 
		except:
			item["seller_name"] = "ERROR"


		#get seller registration month+year
		try:
			seller_history_raw = soup.find_all("span", {"class": "user_since"})
			seller_history = seller_history_raw[0].get_text().replace("Torin käyttäjä", "").strip()

			print(seller_history) #elokuusta 2020
			item["seller_since_original"] = seller_history 

			seller_history_time_raw = re.search(r'(.*)\s([0-9]+)', seller_history)

			seller_history_time = "{}-{}".format(seller_history_time_raw.group(2), months_elative[seller_history_time_raw.group(1)])

			print(seller_history_time)
			item["seller_since_year_month"] = seller_history_time
		except:
			item["seller_since_original"] = "ERROR"
			item["seller_since_year_month"] = "ERROR"


		#add seller_quasi_id for easier indefitication
		try:
			item["seller_quasi_id"] = "{}_{}".format(item["seller_name"], item["seller_since_year_month"])

		except:
			item["seller_quasi_id"] = "ERROR"


		#get seller location county+city
		try:
			seller_location_raw = soup.find_all("div", {"class": "nohistory private"})
			seller_location_raw2 = re.search(r'(.*)[.\s]+<br/>(.*)[.\s]+</div>$', str(seller_location_raw[0]))
			seller_location_county = seller_location_raw2.group(1).strip()
			seller_location_city = seller_location_raw2.group(2).strip()

			print(seller_location_county)
			item["seller_county"] = seller_location_county

			print(seller_location_city)
			item["seller_city"] = seller_location_city
		except:
			item["seller_county"] = "ERROR"
			item["seller_city"] = "ERROR"


		item["pics_hrefs"] = ""

		try:
			#download pictures if any
			i = 1

			if int(item["pics_count"]) > 0:

				pics_spans = soup.find_all("span", {"class": "thumb_link"})

				temp_pics_hrefs = []

				for link in pics_spans:
					pic_href = link["href"].replace("medium_660", "big")
					print(pic_href)
					temp_pics_hrefs.append(pic_href)

					urllib.request.urlretrieve(pic_href, "data_pictures/{}_{:02d}.jpg".format(item["id"], i))

					i = i + 1

					time.sleep(random.randint(50, 120)/100)

				item["pics_hrefs"] = ", ".join(temp_pics_hrefs)
		except:
			item["pics_hrefs"] = "ERROR"
			

		#add to spreadsheet

		new_row = (item["id"], item["title"], item["relevant"], item["price"], item["price_now"], item["price_previous"], item["category"], item["diameter"], item["published_date_time"], item["published_original"], item["seller_name"], item["seller_quasi_id"], item["seller_since_year_month"], item["seller_since_original"], item["seller_county"], item["seller_city"], item["description"], item["href"], item["pics_count"], item["pics_hrefs"])

		sheet_instance.append_row(new_row)

		time.sleep(random.randint(120, 210)/100)


print("")
print("")
print("")
print(data)
